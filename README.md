# EXPERT
Il s'agit de l'application liée au projet Volo.

Ce squelette (template) peut être utilisé pour les activités
* exg5521v : squelette pour le choix individuel des expertises (execution en local)
  * mvn spring-boot:run
* exg9252 : squelette pour un déploiement sur Heroku par une commande maven (pré-requis : exa9314 - établissement d'un compte Heroku)
  * mvn clean heroku:deploy

# Note pour le prof
 - master initial in github@prodageo/exg5020
