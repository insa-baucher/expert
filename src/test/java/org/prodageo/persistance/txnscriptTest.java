// dépendances au cadre de test JUnit
import org.junit.jupiter.api.Test ;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

// ajout des dépendances aux classes de l'application
import org.prodageo.persistance.txnscript ;
import java.sql.ResultSet;

// classes utilitaires
import java.util.Date;
import java.text.SimpleDateFormat;

@TestMethodOrder(OrderAnnotation.class) // l'ordre d'exécution n'est pas celui du fichier par défaut :(
class txnscriptTest {

    public static txnscript txn = new txnscript() ;
    public static int nbLignesAuDemarrage = 3 ;

    // LISTE ORDONNEE DES SCENARIO DE TESTS
    @Test
    @Order(1)
    void verifierNbLignesConformeDansResultSetAuDemarrage() throws Exception
    {
		// test de la fonction remonterEnrAssocies
        int nbLignesAttendu = nbLignesAuDemarrage ;

        // on suppose que la table est peuplée avec 3 enregistrements au démarrage de Mickey à Junior
        ResultSet rs = txn.remonterEnrAssocies() ;
        // compter le nombre de lignes du tableau (ResultSet n'a pas de fonction size() !!!)
        // source : https://stackoverflow.com/questions/192078/how-do-i-get-the-size-of-a-java-sql-resultset
        int nbLignes = 0;
        while(rs.next()) {
            nbLignes++;
        }
        // primitive de comparaison qui gère toute la tuyauterie d'affichage des résultats de tests
        assertEquals( nbLignesAttendu , nbLignes ) ;
    }

    @Test
    @Order(2)
    void verifierNbAssocies() throws Exception
	{
		// test équivalent au précédent (c'est à dire avoir le nombre de lignes de la table Person mais avec une autre méthode !
		int nbAssocies = txn.obtenirNbAssocies () ;
		assertEquals( nbLignesAuDemarrage , nbAssocies ) ;
	}

}
