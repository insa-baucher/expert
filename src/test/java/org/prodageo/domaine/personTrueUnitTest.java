// dépendances au cadre de test JUnit
import org.junit.jupiter.api.Test ;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

// ajout des dépendances aux classes de l'application
import org.prodageo.persistance.txnscript ;
import org.prodageo.domaine.Person ;


// added for True Unit Test
// https://www.javacodegeeks.com/2015/11/getting-started-with-mockito.html
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.runner.RunWith;
import org.mockito.Mock;
// import org.mockito.runners.MockitoJUnitRunner; // deprecated ?

// https://stackoverflow.com/questions/44200720/difference-between-mock-mockbean-and-mockito-mock
// Mockito tests in Springboot
// import org.mockito.MockBean;

import java.util.Date;
import java.time.LocalDate;

// @RunWith(MockitoJUnitRunner.class) // added for True Unit Test
class personTrueUnitTest {

  // @Mock // added for True Unit Test
    private txnscript txn = new txnscript() ;
    private txnscript mockedTxn = org.mockito.Mockito.mock(txnscript.class);




    @Test
    void verifierAge() throws Exception
    {
        int ageAttendu = 21 ;

		Person p = txn.obtenirAssocie(1) ; // p est Mickey, né en 2000 => comparé à aujourd'hui en 2021 = 21 ans
		int ageObserve = p.getAge() ;
    
        assertEquals( ageAttendu , ageObserve ) ;
    }
	
    @Test
    void verifierAgeUnitairement() throws Exception
    {
        int ageAttendu = 22 ;

		LocalDate birthDate = LocalDate.of(1999, 3, 1);
		Person mockedPerson = new Person( "Mickey", "DISNEY", birthDate) ;
		when(mockedTxn.obtenirAssocie(1)).thenReturn(mockedPerson);
		
		Person p = mockedTxn.obtenirAssocie(1) ; // p is mockedPerson !
		int ageObserve = p.getAge() ;
    
        assertEquals( ageAttendu , ageObserve ) ;
    }	
}