// dépendances au cadre de test JUnit
import org.junit.jupiter.api.Test ;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

// ajout des dépendances aux classes de l'application
import org.prodageo.persistance.txnscript ;
import org.prodageo.domaine.Person ;


@TestMethodOrder(OrderAnnotation.class) // l'ordre d'exécution n'est pas celui du fichier par défaut :(
class personTest {

    public static txnscript txn = new txnscript() ;

    // LISTE ORDONNEE DES SCENARIO DE TESTS
    @Test
    @Order(1)
    void verifierAge() throws Exception
    {
    // id = 1 => birthDate = 2000-03-01
        int ageAttendu = 21 ;

        Person p = txn.obtenirAssocie ( 1 ) ;
    int ageObserve = p.getAge() ;
    
        assertEquals( ageAttendu , ageObserve ) ;
    }

}

