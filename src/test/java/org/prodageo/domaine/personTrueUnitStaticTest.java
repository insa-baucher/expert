// dépendances au cadre de test JUnit
import org.junit.jupiter.api.Test ;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;


// added for true unit test with static 
// import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

// import static org.assertj.core.api.Assertions.*;



// added for True Unit Test
// https://www.javacodegeeks.com/2015/11/getting-started-with-mockito.html
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.runner.RunWith;
import org.mockito.Mock;
// import org.mockito.runners.MockitoJUnitRunner; // deprecated

// ajout des dépendances aux classes de l'application
import org.prodageo.persistance.txnscript ;
import org.prodageo.domaine.Person ;
import org.prodageo.domaine.Utilitaires ;


// https://stackoverflow.com/questions/44200720/difference-between-mock-mockbean-and-mockito-mock
// Mockito tests in Springboot
// import org.mockito.MockBean;

import java.util.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

// @RunWith(MockitoJUnitRunner.class) // added for True Unit Test - deprecated ?
class personTrueUnitStaticTest {

	@Mock // added for True Unit Test
    // private txnscript txn ; // = new txnscript() ;
	private txnscript txn = org.mockito.Mockito.mock(txnscript.class);

/*
    @Test
    void verifierAgeUnitairementFake() throws Exception
    {
		assertEquals( 1 , 2 ) ;
	}
*/

    @Test
    void verifierAgeUnitairementStatic() throws Exception
    {
        int ageAttendu = 27 ;

		// https://www.baeldung.com/mockito-mock-static-methods
		// https://frontbackend.com/java/how-to-mock-static-methods-with-mockito
		// to mock the static Utilities class that returns the current
		LocalDate maintenant = LocalDate.of(2025, 11, 15);
		try (MockedStatic<Utilitaires> utilities = Mockito.mockStatic(Utilitaires.class)) {
			utilities.when( () -> Utilitaires.Maintenant()).thenReturn(maintenant);


			// https://stackoverflow.com/questions/28177370/how-to-format-localdate-to-string : print the year of maintenant()
			/*
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy");
			String formattedString = Utilitaires.Maintenant().format(formatter);
			System.out.println(formattedString);
			
			assertEquals(2021, formattedString);
			*/

		
			LocalDate birthDate = LocalDate.of(1998, 3, 1);
			Person mockedPerson = new Person( "Mickey", "DISNEY", birthDate) ;
			when(txn.obtenirAssocie(1)).thenReturn(mockedPerson);
			

			
			Person p = txn.obtenirAssocie(1) ; // p is mockedPerson !
			int ageObserve = p.getAge() ;
			
			assertEquals( ageAttendu , ageObserve ) ;
		}
    }
}