// dépendances au cadre de test JUnit
import org.junit.jupiter.api.Test ;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;


// added for true unit test with static 
// import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

// import static org.assertj.core.api.Assertions.*;



// added for True Unit Test
// https://www.javacodegeeks.com/2015/11/getting-started-with-mockito.html
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.runner.RunWith;
import org.mockito.Mock;
// import org.mockito.runners.MockitoJUnitRunner; // deprecated

// ajout des dépendances aux classes de l'application
import org.prodageo.persistance.txnscript ;
import org.prodageo.domaine.Person ;
import org.prodageo.domaine.Utilitaires ;
import org.prodageo.domaine.Address ;


// https://stackoverflow.com/questions/44200720/difference-between-mock-mockbean-and-mockito-mock
// Mockito tests in Springboot
// import org.mockito.MockBean;

import java.util.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

// @RunWith(MockitoJUnitRunner.class) // added for True Unit Test - deprecated ?
class personTrueUnitReferenceTest {

  @Mock // added for True Unit Test
    // private txnscript txn ; // = new txnscript() ;
  private txnscript txn = org.mockito.Mockito.mock(txnscript.class);

    @Test
    void verifierDistanceLieAuNombreDeDeplacement() throws Exception
    {

    // la fonction Address.distance() n'est pas prête
    // on suppose que la distance entre le domicile principal de l'associe et la maison partagee est de 500 km
        int distanceAttendu = 1500 ;
		int distanceObserve = 0 ;
		int nbDeplacement = 3 ;

		Address theAddress = new Address ( 75000, "PARIS" ) ;

		// la fonction Address.distance() est boguée, elle n'est pas prête, comment faire ?

		LocalDate theBirthDate = LocalDate.of(1999, 3, 1);
		Person p = new Person ( "Donald", "Duck", theBirthDate, theAddress) ;
		distanceObserve = p.distance ( nbDeplacement ) ;

		assertEquals( distanceAttendu , distanceObserve ) ;

    }



    @Test
    void verifierDistanceLieAuNombreDeDeplacementUnitairement() throws Exception
    {

    // la fonction Address.distance() n'est pas prête
    // on suppose que la distance entre le domicile principal de l'associe et la maison partagee est de 500 km
        int distanceAttendu = 1500 ;
		int distanceObserve = 0 ;
		int nbDeplacement = 3 ;

		// To mock an object (Address) that is referenced by the SUT
		// and forge the response of the distance function
		Address anAddress = org.mockito.Mockito.mock(Address.class);

		// la fonction Address.distance() n'est pas prête

		
		// je force la valeur du code postal à 75000 pour l'adresse mocked
		when(anAddress.getZipCode()).thenReturn(75000);
		
		// on suppose que la distance entre le domicile principal (75000) de l'associe et la maison partagee (83000) est de 500 km
		when(anAddress.distance(83000)).thenReturn(500);

		// distanceObserve = anAddress.distance(75000) ;
		// assertEquals( distanceAttendu , distanceObserve ) ;

		LocalDate aBirthDate = LocalDate.of(1999, 3, 1);
		Person p = new Person ( "Donald", "Duck", aBirthDate, anAddress) ;
		distanceObserve = p.distance ( nbDeplacement ) ;

		// distanceObserve = p.getZipCode ( ) ;

		assertEquals( distanceAttendu , distanceObserve ) ;

    }
}
