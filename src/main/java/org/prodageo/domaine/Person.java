package org.prodageo.domaine;

import java.util.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.text.SimpleDateFormat;
import java.time.Period ;
import org.prodageo.domaine.Address ;

// classe du domaine
public class Person {
 
    private String firstName;
    private String lastName;
  private LocalDate birthDate ;
  private Address personAddress ;
 
    public Person() {
 
    }
 
    public Person(String firstName, String lastName, LocalDate birthDate, Address personAddress ) {
        this.firstName = firstName;
        this.lastName = lastName;
    this.birthDate = birthDate;
    this.personAddress = personAddress ;
    }
 
     public Person(String firstName, String lastName, LocalDate birthDate) {
        this.firstName = firstName;
        this.lastName = lastName;
    this.birthDate = birthDate;
    }
 
    public String getFirstName() {
        return this.firstName ;
    }
 
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
 
    public String getLastName() {
        return this.lastName;
    }
 
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
  
    public void setBirthDate (LocalDate birthDate) {
        this.birthDate = birthDate;
    }
  
    public int getZipCode() {
        return this.personAddress.getZipCode();
    }
	
    public int getAge() {
    // https://stackoverflow.com/questions/29750861/convert-between-localdate-and-sql-date
    LocalDate currentDate = Utilitaires.Maintenant();    
    return Period.between(this.birthDate, currentDate).getYears();
    }
  
  // pour calculer le nombre de km déplacement à rembourser à l'Associe 
  // en fonction du nombre de fois où il a du se rendre dans la maison pour y faire des travaux.
  public int distance(int nbDeplacement)
  {
    int otherZipCode = Utilitaires.codePostalMaisonPartagee () ;
    
    if ( this.personAddress != null )
    {
      return nbDeplacement * ( this.personAddress.distance( otherZipCode ) ) ;
    }
    else
    {
      return 1000 ;
    }

  }
 
}