package org.prodageo.domaine;

import java.util.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.text.SimpleDateFormat;
import java.time.Period ;

// classe du domaine
public class Address {
 
    private int zipCode ;
    private String city ;
 
    public Address() {
 
    }
 
    public Address(int zipCode, String city) {
        this.zipCode = zipCode;
        this.city = city;
    }
	
	public int getZipCode ( )
	{
		return this.zipCode ;
	}
 
    // calculate distance between the city of the adress and another city given by zipCode.
    public int distance(int otherZipCode) {
		//weird solution
		int thisZipCode = getZipCode () ;
		
		if ( otherZipCode > thisZipCode )
		{
		  return otherZipCode - thisZipCode ;
		}
		else
		{
		  return thisZipCode - otherZipCode ;
		}
	
    }
 
 
}