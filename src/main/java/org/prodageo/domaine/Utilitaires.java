package org.prodageo.domaine;

import java.util.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.text.SimpleDateFormat;
import java.time.Period ;

// classe du domaine
public class Utilitaires {
 
  private Utilitaires() {}
  
    public static LocalDate Maintenant() {
    // https://stackoverflow.com/questions/29750861/convert-between-localdate-and-sql-date
    Date now = new Date();
    return now.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();    
    }
  
    public static int codePostalMaisonPartagee()
  {  
    return 83000 ;
  }

}